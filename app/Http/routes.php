<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI  is requested.
|
*/

Route::get('/', function () {
    return view('dashboard.login');
});



Route::group(['middleware' => 'auth'], function()
{
Route::get('dash','baseControler@index');
Route::get('PhoneApp','baseControler@PhoneApp');
Route::get('Incoming','baseControler@Incoming');
Route::get('Outgoing','baseControler@Outgoing');
Route::get('voicemail','baseControler@settingslist');
Route::get('newsetting','baseControler@voicemail');
Route::post('voicemail','baseControler@savevoicemailconfig');
Route::get('editsetting/{id}','baseControler@editsettings');
Route::post('editsetting/{id}','baseControler@updatesettings');
Route::get('business/{callerId?}','business@GetData');
});

Route::any('incoming.php','baseControler@incominghandler');
Route::any('outgoing.php','baseControler@outgoinghandler');
Route::any('voicemail.php', ['as' => 'voicemail.php', 'uses' => 'baseControler@voicemailhandler']);
Route::post('emailnotify', ['as' => 'emailnotify', 'uses' => 'baseControler@emailnotify']);
Route::post('checkUser','logincontroler@loginProces');
Route::get('login','logincontroler@login');
