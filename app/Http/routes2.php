<?php
use App\Http\Sessions;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('dashboard.login');
});
Route::get('check-session','UsersController@sessionExpire');
Route::group(['middleware' => 'auth'], function()
{
  //dd(session('onSightlocation'));

  Route::get('auth/logout', 'Auth\AuthController@getLogout');
  Route::get('onSightView', 'onsightViewController@index');

});
Route::post('checkUser','logincontroler@loginProces');
Route::get('login','logincontroler@login');
/*Route::get('imgeupload','imageUploadControler@imgeupload');
Route::post('apply/upload', 'ApplyController@upload');
Route::get('chart', 'StatsController@getIndex');
Route::get('api', 'StatsController@getApi');*/
