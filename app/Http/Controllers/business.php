<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\business_trac;
class business extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetData($callerId =0)
    {
if(  preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', $callerId,  $matches ) )
{
    $result = "(".$matches[1].") "  .$matches[2] . '-' . $matches[3];
}
       $caller = business_trac::where('phone', $result)->first();
	if (! $caller) { return "Incoming call from ". $result ." </br> No caller record found";}

echo "<table class='table table-striped table-bordered'>";
echo "<tr>";
foreach (array_keys($caller->toArray()) as $column_name) {
echo  "<th><strong>". $column_name ."</strong></th>";
  }
echo  " </tr>";

foreach ($caller->toArray() as $item){
// echo  " <tr>";
// foreach($item as $value){
 echo  " <td>". $item ."</td>";
//    }
//echo   " </tr>";
}
echo "</table>";

//	return $caller;   
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
