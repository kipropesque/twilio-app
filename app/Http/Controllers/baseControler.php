<?php
namespace App\Http\Controllers;
use App\Http\Controllers\ReportController;
use App\customer;
use App\Users;
use App\Setting;
use App;
use DB;
use App\radacct;
use App\Http\useragent;
use App\accesspoint;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use DateTimeZone;
use Input;
use Carbon\Carbon;
use Twilio\Jwt\ClientToken;
use Twilio\Rest\Client;
use Twilio\Twiml;
use Mail;
use URL;

class baseControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $total_accessPoint;
    public function index()
    {

	// put your Twilio API credentials here
      $accountSid = env('TWILLIO_ACCOUNT_SID');
      $authToken  = env('TWILLIO_AUTH_TOKEN');
      $appSid = env('TWILLIO_APP_SID');

     $capability = new ClientToken($accountSid, $authToken);
      $capability->allowClientOutgoing($appSid);
      $capability->allowClientIncoming('fazal');
      $token = $capability->generateToken();
        return view('dashboard.dash')->with('token',$token);
    }

    public function PhoneApp()
    {
      // put your Twilio API credentials here
      $accountSid = env('TWILLIO_ACCOUNT_SID');
      $authToken  = env('TWILLIO_AUTH_TOKEN');
      $appSid = env('TWILLIO_APP_SID');

     $capability = new ClientToken($accountSid, $authToken);
      $capability->allowClientOutgoing($appSid);
      $capability->allowClientIncoming('fazal');
      $token = $capability->generateToken();


      return view('dashboard.PhoneApp')->with('token',$token);
    }

    public function Incoming()
    {
      // put your Twilio API credentials here
      $accountSid = env('TWILLIO_ACCOUNT_SID');
      $authToken  = env('TWILLIO_AUTH_TOKEN');
      $appSid = env('TWILLIO_APP_SID');      

      $capability = new ClientToken($accountSid, $authToken);
      $capability->allowClientOutgoing($appSid);
      $capability->allowClientIncoming('fazal');
      $token = $capability->generateToken();

      $client = new Client($accountSid, $authToken);

//       foreach ($client->calls->read() as $call) {
//     echo "To: ".$call->to."  From: ".$call->from;
// }

        return view('dashboard.Incoming')->with('token',$token)->with('calls',$client->calls->read());
    }

    public function Outgoing()
    {
      // put your Twilio API credentials here
      $accountSid = env('TWILLIO_ACCOUNT_SID');
      $authToken  = env('TWILLIO_AUTH_TOKEN');
      $appSid = env('TWILLIO_APP_SID');
      
      $capability = new ClientToken($accountSid, $authToken);
      $capability->allowClientOutgoing($appSid);
      $capability->allowClientIncoming('fazal');
      $token = $capability->generateToken();
      $client = new Client($accountSid, $authToken);

//       foreach ($client->calls->read() as $call) {
//     echo "To: ".$call->to."  From: ".$call->from;
// }

        return view('dashboard.Outgoing')->with('token',$token)->with('calls',$client->calls->read());
    }

    public function settingslist()
    {
      $settings = Setting::all();
      return view('dashboard.settingslist')->with('settings', $settings);
    }

    public function voicemail()
    {
      $settings = Setting::all();
      return view('dashboard.Voicemail');
    }

    public function incominghandler()
    {
      $setting = $this->getSetting();
      if($setting != null){
        $response = new Twiml;
        $dial = $response->dial(['timeout' => $setting->delay, 'action' => route('voicemail.php')]);
        $dial->client('fazal');
        return response($response)->header('Content-Type', 'text/xml');
      }
      else {
        $response = new Twiml;
        $dial = $response->dial(['action' => route('voicemail.php')]);
        $dial->client('fazal');
        return response($response)->header('Content-Type', 'text/xml');
      }
    }

    public function outgoinghandler(Request $request){
        $callerId = '6786190771';
        $phoneNumber = $request->PhoneNumber;

        $response = new Twiml;

        // get the phone number from the page request parameters, if given
        if (isset($phoneNumber)) {
            $number = htmlspecialchars($phoneNumber);
            $response->dial(array(
                'callerId' => $callerId
            ))->number($number);
        } else {
            $response->say("Thanks for calling!");
        }

        echo $response;
    }

    public function voicemailhandler(Request $request)
    {
      $setting = $this->getSetting();
      if($setting != null){
        $response = new Twiml();
        $callStatus = $request->input('DialCallStatus');

        if ($callStatus !== 'completed') {
            $response->play(URL::to('/voice/'.$setting->voicemailpath));

            $response->record([
                 'method' => 'POST',
                 'action' => route('emailnotify')]
                 );

            $response->hangup();

            return response($response)->header('Content-Type', 'text/xml');
        }
        return "Ok";
      }
    }

    public function emailnotify(Request $request)
    {
      $setting = $this->getSetting();
      if($setting != null){
        $email = $setting->email;
        Mail::send('newrecording', ['link' => $request->RecordingUrl], function ($m) use ($email){
              $m->from('admin@digitalmarketingxprts.com', 'Phone app notification');
              $m->to($email)->subject('You have new voicemail!');
          });
      }
    }

    public function savevoicemailconfig(Request $request)
    {
      //validate
      $this->validate($request, [
        'voicefile' => 'required',
        'seconds' => 'required|numeric|between:5,120',
        'sttime' => 'required',
        'endtime' => 'required',
        'email' => 'required|email',
      ]);

      //store voice
      if ($request->hasFile('voicefile')) {
        $filename = $request->voicefile->getClientOriginalName();
        $request->file('voicefile')->move(public_path() . '/voice', $filename);
      }

      //save
      $settings = new Setting();
      $settings->voicemailpath = $request->voicefile->getClientOriginalName();
      $settings->start_time = $request->sttime;
      $settings->end_time = $request->endtime;
      $settings->email = $request->email;
      $settings->delay = $request->seconds;
      $settings->save();
      return view('dashboard.Voicemail');
    }
    
    public function editsettings($id)
    {
      $setting = Setting::find($id);
      return view('dashboard.editvoicemail')->with('setting', $setting);
    }

    public function updatesettings(Request $request, $id)
    {
      //validate
      $this->validate($request, [
        'voicefile' => 'required',
        'seconds' => 'required|numeric|between:5,120',
        'sttime' => 'required',
        'endtime' => 'required',
        'email' => 'required|email',
      ]);

      //store voice
      if ($request->hasFile('voicefile')) {
        $filename = $request->voicefile->getClientOriginalName();
        $request->file('voicefile')->move(public_path() . '/voice', $filename);
      }

      //save
      $settings = Setting::find($id);
      $settings->voicemailpath = $request->voicefile->getClientOriginalName();
      $settings->start_time = $request->sttime;
      $settings->end_time = $request->endtime;
      $settings->email = $request->email;
      $settings->delay = $request->seconds;
      $settings->save();

      $settings = Setting::all();
      return view('dashboard.settingslist')->with('settings', $settings);
    }

    public function getSetting()
    {
      date_default_timezone_set('America/New_York');
      //Get all settings
      $settings = Setting::all();
      //Check first valid setting for current time
      foreach ($settings as $key => $setting) {
        $f = DateTime::createFromFormat('H:ia', $setting->start_time);
        $t = DateTime::createFromFormat('H:ia', $setting->end_time);
        $ir = new DateTime;
        if ($f > $t){ 
          $t->modify('+1 day');
        }
        if( ($f <= $ir && $ir <= $t) || ($f <= $ir->modify('+1 day') && $ir <= $t) ){
          return $setting;
        }
      }
      return null;
    }
}
