<script type="text/javascript" src="{{ URL::asset('dist/plugins/chartjs/Chart.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('https://www.google.com/jsapi') }}"></script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["geochart"]});
      google.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {

        var data = google.visualization.arrayToDataTable([
          ['Country', 'Popularity'],

          ['New Zealand', 100],
          ['Singapore', 300]

        ]);
        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('visualization'));

        chart.draw(data, options);
  go();

  window.addEventListener('resize', go);

  function go() {

    chart.draw(data, options);

  }

      }

    </script>
    <style>
    #visualization {
      position: relative;
      width: 100%;
      max-height:280px;
    }
    </style>

    <script>
		var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

		var lineChartData = {

			labels : [@foreach($chartData as $day)"{{$day['day']}}",@endforeach],
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [@foreach($chartData as $day)"{{$day['totalData']}}",@endforeach]
				}
			]
		}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData, {
			responsive: true
		});
	}
	</script>
  <div class="col-md-8">
<div class="box">
<div class="box-body">
<span class="box-title"><b>Total sessions report</b></span>
                  <div class="graph">
      <canvas id="canvas" height="300" width="600"></canvas>
                  </div><!-- /.chart-responsive -->
                </div>
      </div>
      </div>
