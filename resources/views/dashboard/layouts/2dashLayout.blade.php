<!DOCTYPE html>
<html>
  @include('dashboard.common.header')
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="dash" class="logo hidden-xs">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">{!!Html::image('dist/images/logo.png', 'a logo', array('style' => 'height:30px;margin:5px'))!!}</span>
          <!-- logo for regular state and mobile devices -->
          <span class="hidden-xs">{!!Html::image('dist/images/logo.png', 'a logo', array('style' => 'height:40px;margin:5px'))!!}</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">

		  <a href="index.php" class="hidden-sm hidden-md hidden-lg">
		<!--img src="images/logo.png" class="hidden-sm hidden-md hidden-lg" style="height:30px;margin:5px;right:45%;position:absolute;margin-top:10px;"--->
		</a>
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!-- Notifications: style can be found in dropdown.less -->
              <!-- Tasks: style can be found in dropdown.less -->
              <!-- User Account: style can be found in dropdown.less -->

              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-bottom: 15px; padding-top: 15px;">
                  {!!Html::image('dist/images/user.jpg', 'user image', array('class' => 'user-image'))!!}
                  <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>

                <ul class="dropdown-menu">

                  <!-- User image -->
                  <li class="user-header">
                    {!!Html::image('dist/images/user.jpg', 'user image', array('class' => 'img-circle'))!!}
                    <p>
                    {{ Auth::user()->name }}                     <!--<small></small>-->
                    </p>
                  </li>
                  <!-- Menu Body -->

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="setting" class="btn btn-default btn-flat">Settings</a>
                    </div>
                    <div class="pull-right">
                      <a href="auth/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" style="background:#DBE1E6;">
        <section class="sidebar" style="height: auto;">
      @include('dashboard.common.sidebarLeft')
    </section>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            @yield('headear_title')
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        @yield('content')
        <!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <!--b>Version</b> 2.0------>
        </div>
        <strong>Copyright &copy;  <a href="https://www.wifigen.xyz/">Wifigen LLC</a></strong>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->

      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    {!!HTML::script('dist/plugins/jQuery/jQuery-2.1.4.min.js')!!}
    @yield('activeUserJs')
    <!-- Bootstrap 3.3.2 JS -->
    {!!HTML::script('dist/js/bootstrap.min.js')!!}
    <!-- FastClick -->
    {!!HTML::script('dist/plugins/fastclick/fastclick.min.js')!!}
    <!-- AdminLTE App -->
    {!!HTML::script('dist/dist/js/app.min.js')!!}
    <!-- Sparkline -->
    {!!HTML::script('dist/plugins/sparkline/jquery.sparkline.min.js')!!}
    <!-- jvectormap -->
    {!!HTML::script('dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}
    {!!HTML::script('dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!}
    <!-- SlimScroll 1.3.0 -->
    {!!HTML::script('dist/plugins/slimScroll/jquery.slimscroll.min.js')!!}
    <!-- ChartJS 1.0.1 -->
    @yield('dash_jsCharts')
    @yield('js_dataTable')
    <!-- AdminLTE for demo purposes -->
    {!!HTML::script('dist/dist/js/demo.js')!!}
      @yield('js_select')
  </body>
</html>
