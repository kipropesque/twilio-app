<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to Punjab Wifi.</title>
	{!!Html::style('dist/onSightView/css/bootstrap.min.css')!!}
	{!!Html::style('dist/onSightView/css/font-awesome.css')!!}
	{!!Html::style('dist/onSightView/css/animate.css')!!}
	{!!Html::style('dist/onSightView/css/style.css')!!}
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title> Welcome to Punjab Wifi.</title>
    <style>
      /* Always set the map height explicitly  to define the size of the div
       * element that contains the  map . */
       .ibox-content > h1 {
   font-size: 42px;
}
.ibox-content{font-weight:bold;}
.ibox-content > small {font-size:100%;}
.ibox-title > h5 {font-weight:700;font-size:16px;}
.ibox-content .stat-percent {
font-size: 16px;}

      /* Fixes for android internal browser */
       .heading
       {
          margin-top: 275px !important;
       }

       .ibox.float-e-margins
       {
         margin-bottom: 18px !important;
       }
       .ibox.edit1
       {
         margin-top: 10px !important;
       }

       /* Fixes End */


       @media screen and (max-width: 1281px)
{

      .ibox-title h5 {

        font-size: 15px;
        margin: 5px 0px 0px 0px;

    }
    .heading {

        margin-top: 20px;
    }

    .ibox-title {

        margin-bottom: -8px;
        padding: 13px 15px 0px 15px;

    }

    .ibox-content {

      padding: 16px 20px 14px 20px;

    }
    .wrapper-content {
        padding: 2% 2% 0% 2% !important;
    }
    .edit1 {
        margin-top: 24px !important;

    }
    .edit {
        margin-top: -166px;

    }
    .ibox-content {

      min-height: auto;

    }
    .heading {

   padding-top: 1rem;
   padding-bottom: 1rem;
}
}
        #map
        {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body
        {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #page-wrapper
        {
            margin: 0px;
            min-height: 0px;
            height:90vh;
            padding-left: 0px;
            padding-right: 0px;
        }

        .navbar
        {
            margin-bottom: 0px;

        }

        .brand-logo
        {
            height: 50px;
            margin-left: 22px;
            margin-top: 4px;
        }

        .location-identification
        {
            font-weight: bold;
            color: white;
        }

      .dropdown-menu > li > a
      {
          line-height: 12px;
      }

      .float-e-margins
      {
          -webkit-box-shadow: 23px 19px 34px -12px;
      }

      .footer
      {
          position: fixed;
      }
        .wrapper-content
        {
            padding: 2%;
        }

        .progress-bar-usage
        {
            margin-top: 11%;
        }

        .usage-bar
        {
            height: 25px !important;
            margin-bottom: 1.5%;
        }

        .usage-bar-heading
        {
            margin-bottom: 1.5%;
        }


        .heading-block
        {

            margin-bottom: 3%;
            background: transparent;
            background-color: gray;
            border-radius: 5px;
            opacity: 0.2;

        }

        .heading
        {
            color: #293846;
            font-weight: bold;
            font-size: 2.5rem;
            padding-top: 2rem;
            padding-bottom: 2rem;
        }
		.edit
		{ margin-top: -171px;
		-webkit-box-shadow: 23px 19px 34px -12px;
		}
		.edit1{ margin-top: 32px;
		-webkit-box-shadow: 23px 19px 34px -12px;}

    </style>
  </head>
  <body>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation">
                <ul class="nav navbar-top-links navbar-left">
                <li>
                <i>{!!Html::image('dist/images/onsightLogo.png', 'a logo', array('class' => 'brand-logo'))!!}</i>
                </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right">

                    <li class="location-identification">
                        <i class="fa fa-location-arrow"></i> {{$location}}.   &nbsp;  &nbsp;
                    </li>
                    <li>
                        <a href="#" style="color:#293846;cursor: default;">
                            <i class="fa fa-sign-out" style="color:#293846;cursor: default;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    <div id="map" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; background-color: rgb(229, 227, 223);"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;" class="gm-style"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;" aria-hidden="true"><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 768px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1024px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 768px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 768px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1024px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1024px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 512px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1280px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 512px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 512px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1280px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1280px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 768px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 768px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1024px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1024px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 512px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 512px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1280px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1280px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1536px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1536px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1536px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1536px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1536px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1792px; top: 354px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1792px; top: 98px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1792px; top: 610px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 866px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1792px; top: -158px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1792px; top: 866px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;" aria-hidden="true"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 768px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1024px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 768px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 768px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1024px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1024px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1280px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1280px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1280px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 768px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 768px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1024px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1024px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1280px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1280px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1536px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1536px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1536px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1536px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1536px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1792px; top: 354px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1792px; top: 98px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1792px; top: 610px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 866px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1792px; top: -158px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1792px; top: 866px;"></div></div></div><div style="width: 22px; height: 40px; overflow: hidden; position: absolute; left: 664px; top: 255px; z-index: 295;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;" aria-hidden="true"><div style="position: absolute; left: 0px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 768px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1024px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 768px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 768px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1024px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1024px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 512px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1280px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 512px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 512px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1280px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1280px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 768px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 768px; top: 866px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1024px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1024px; top: 866px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 256px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 512px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 512px; top: 866px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1280px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1280px; top: 866px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1536px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 256px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 256px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1536px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1536px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 256px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 256px; top: 866px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1536px; top: -158px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1536px; top: 866px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 0px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 1792px; top: 354px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 0px; top: 98px; transition: opacity 200ms ease-out 0s;"></div><div style="position: absolute; left: 0px; top: 610px; transition: opacity 200ms ease-out 0s;"></div><e"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); cursor: pointer; background-color: rgb(255, 255, 255); display: none;" title="Rotate map 90 degrees"><img style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;" src="Welcome%20to%20Punjab%20Wifi_files/tmapctrl4.png" draggable="false"></div><div style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);" class="gm-tilt"><img style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none;" src="Welcome%20to%20Punjab%20Wifi_files/tmapctrl4.png" draggable="false"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div style="float: left;" class="gm-style-mtc"><div style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); min-width: 22px; font-weight: 500;" draggable="false" title="Show street map">Map</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); position: absolute; left: 0px; top: 32px; text-align: left; display: none;"><div style="color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;" draggable="false" title="Show street map with terrain"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img style="position: absolute; left: -52px; top: -44px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;" src="Welcome%20to%20Punjab%20Wifi_files/imgs8.png" draggable="false"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div style="float: left;" class="gm-style-mtc"><div style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); border-left: 0px none; min-width: 40px;" draggable="false" title="Show satellite imagery">Satellite</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: 0px 1px 4px -1px rgba(0, 0, 0, 0.3); position: absolute; right: 0px; top: 32px; text-align: left; display: none;"><div style="color: rgb(0, 0, 0); font-family: Roboto,Arial,sans-serif; -moz-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;" draggable="false" title="Show imagery with street names"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img style="position: absolute; left: -52px; top: -44px; -moz-user-select: none; border: 0px none; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;" src="Welcome%20to%20Punjab%20Wifi_files/imgs8.png" draggable="false"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div></div></div></div>
     <div class="wrapper wrapper-content">
       <div class="row">
           <div class="col-lg-offset-3 col-lg-6">
               <h3 class="heading text-center" style="background-color: white;-webkit-box-shadow: 16px 38px 26px -30px;border: solid #ededed ;">Welcome to Punjab WiFi</h3>
               <!--<div class="heading-block">
               </div>-->
           </div>
           <div class="col-lg-3">
               &nbsp;
               <!-- Empty Space-->
           </div>
       </div>
         <br>
        <div class="row">
           <div class="col-lg-3">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <span class="label label-primary pull-right">Now</span>
                      <h5>Online Users  </h5>
                  </div>
                  <div class="ibox-content">
                      <h1 id="totalActiveUser" class="no-margins">{{$onsightData['totalActiveUser']}} <b></b></h1>
                      <div id="todayTotalUsers" class="stat-percent font-bold text-navy">{{$onsightData['todayTotalUsers']}} </div>
                      <small>Total Users (today)</small>
                  </div>
              </div>
          </div>

          <div class="col-lg-3">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Total Users</h5>
                  </div>
                  <div class="ibox-content">
                      <h1 id="TotalUsers" class="no-margins">{{$onsightData['TotalUsers']}} <b></b></h1>
                      <div id="persuerTime" class="stat-percent font-bold text-navy">{{$onsightData['persuerTime']}} </div>
                      <small>Time spent (Per User)</small>
                  </div>
              </div>
          </div>
          <div class="col-lg-3">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <span class="label label-primary pull-right">Today</span>
                      <h5>Consumed Bandwidth</h5>
                  </div>
                  <div class="ibox-content">
                      <h1 id="totalConsumeDatadaily" class="no-margins">{{$onsightData['totalConsumeDatadaily']}}</h1>
                      <div id="todayPerusageData" class="stat-percent font-bold text-navy">{{$onsightData['todayPerusageData']}} </div>
                      <small>Bandwidth Per connection (Today)</small>
                  </div>
              </div>
          </div>
          <div class="col-lg-3">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Total Bandwidth</h5>
                  </div>
                  <div class="ibox-content">
                      <h1 id="totalConsumeData" class="no-margins">{{$onsightData['totalConsumeData']}}</h1>
                      <div id="perUserConsumeData" class="stat-percent font-bold text-navy">{{$onsightData['perUserConsumeData']}}</div>
                      <small>Bandwidth per connection</small>
                  </div>
              </div>
       </div>
            <br>
            <br>
            <br>
        <div class="row">
            <div class="col-lg-12 wrapper-content progress-bar-usage">
    <div class="ibox-content edit">

                        <!-- Change the  text-navy class to text-danger class for red effect if users are going down -->
                        <div id="stat-percentBandW" class="stat-percent font-bold pull-right" >{{$onsightData['bandWprogressBar']}}% <!---i class="fa text-navy fa-level-up"></i---></div>
                        <h2 class="usage-bar-heading"><b>Data Throughput Capacity.</b></h2>

                        <div class="progress progress-mini usage-bar">

                            <!-- Change this div's width for the width of the progress bar, and the color of the .usage-bar class to change the color of the usage bar -->
                            <div id="BandwidthprogressBar" style="width: {{$onsightData['bandWprogressBar']}}%;background:{{$onsightData['progressBarColor']}}" class="progress-bar usage-bar"></div>
                        </div>

                        <div id="BandwidthprogressBarData" class="m-t-sm ">Bandwidth Per connection (Today) {{$onsightData['todayPerusageData']}} </div>
                    </div>
                <div class="ibox edit1">
                    <div class="ibox-content">

                        <!-- Change the text-navy class to text-danger class for red effect if users are going down -->
                        <div id="averageUserLimt" class="stat-percent font-bold pull-right" >{{$onsightData['averageUserLimt']}}% <!---i class="fa text-navy fa-level-up"></i---></div>
                        <h2 class="usage-bar-heading"><b>Users Online Capacity</b></h2>

                        <div class="progress progress-mini usage-bar">

                            <!-- Change this div's width for the width of the progress bar, and the color of the .usage-bar class to change the color of the usage bar -->
                            <div id="UsersBar" style="width: {{$onsightData['averageUserLimt']}}%;background:{{$onsightData['progressBaruserColor']}}" class="progress-bar usage-bar"></div>
                        </div>

                        <div id="activeUserprogressBar" class="m-t-sm ">Now connected Users {{$onsightData['totalActiveUser']}}</div>
                    </div>
                </div>
            </div>
        </div>
       </div>
       <div class="footer">

            <div>
                <strong>Copyright</strong> Wifigen &copy; 2017
            </div>

        </div>

       </div>
       </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
    // auto reload
    //window.setTimeout(function(){location.reload()},9000)
function getdataOnsight()
{
  $.ajax({
          url:'{{env('BASEURL')}}locationdata/{{$location}}',
          type: 'get',
          //data: {"_token": "{{ csrf_token() }}"},
          success: function(data) {
            if(data != 'same'){
            document.getElementById('totalActiveUser').innerHTML=data['totalActiveUser'];
            document.getElementById('todayTotalUsers').innerHTML=data['todayTotalUsers'];
            document.getElementById('TotalUsers').innerHTML=data['TotalUsers'];
            document.getElementById('persuerTime').innerHTML=data['persuerTime'];
            document.getElementById('totalConsumeDatadaily').innerHTML=data['totalConsumeDatadaily'];
            document.getElementById('todayPerusageData').innerHTML=data['todayPerusageData'];
            document.getElementById('totalConsumeData').innerHTML=data['totalConsumeData'];
            document.getElementById('perUserConsumeData').innerHTML=data['perUserConsumeData'];
            document.getElementById('BandwidthprogressBar').style.width=data['bandWprogressBar']+'%';
            document.getElementById('stat-percentBandW').style.innerHTML=data['bandWprogressBar']+'%';
            document.getElementById('BandwidthprogressBar').style.background=data['progressBarColor'];
            document.getElementById('BandwidthprogressBarData').innerHTML='Bandwidth Per connection (Today) '+data['todayPerusageData'];

            document.getElementById('averageUserLimt').style.innerHTML=data['averageUserLimt']+'%';
            document.getElementById('UsersBar').style.width=data['averageUserLimt']+'%';
            document.getElementById('UsersBar').style.background=data['progressBaruserColor'];
            document.getElementById('activeUserprogressBar').innerHTML='Now connected users '+data['totalActiveUser'];
          }
           console.log(data);
          }
      });
}
window.setInterval(function(){
  getdataOnsight();
}, 5000);
//window.setTimeout(function(){getdataOnsight()},5000);
var marker;

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: {lat: 31.5047, lng: 74.3315}
  });

  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    animation: google.maps.Animation.DROP,
    position: {lat: 31.5047, lng: 74.3315}
  });
  marker.addListener('click', toggleBounce);
}

function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}

	  <!---->




    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYUEzAeevMqCemULGIsyWQiHsXFp0yBGg &callback=initMap"></script>
  </body>
</html>
