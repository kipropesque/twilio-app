@extends('dashboard.layouts.dashLayout')
@section('title','Wifigen | users')
@section('headear_title','User')
@section('user_active','active')
@section('daterangepickercss')
{!!Html::style('dist/plugins/daterangepicker/daterangepicker-bs3.css')!!}
@endsection
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
  <div class="row">
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Popular browser</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkanalytics['popoLBrow']}}</h1>
                          <!----div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                          <small>Total income</small---->
                      </div>
                  </div>
              </div>
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Popular OS</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkanalytics['popoLos']}}</h1>
                          <!----div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                          <small>New orders</small---->
                      </div>
                  </div>
              </div>
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Mobile users</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkanalytics['mob']}}</h1>
                          <!--div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                          <small>New visits</small----->
                      </div>
                  </div>
              </div>
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <!--span class="label label-danger pull-right">Low value</span---->
                          <h5>Laptop users</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkanalytics['com']}}</h1>

                      </div>
                  </div>
      </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Top 10 bandwith consumers </h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="fa fa-wrench"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-user">
                                        <li><a href="#">Config option 1</a>
                                        </li>
                                        <li><a href="#">Config option 2</a>
                                        </li>
                                    </ul>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Occupation</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Internet usage</th>
                                        <th>Registered from</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($bandwidthCosumerUser as $value)
                                    <tr>
                                        <td>{{$value['name']}}</td>
                                        <td>{{$value['phone']}}</td>
                                        <td>{{$value['occupation']}}</td>
                                        <td>{{$value['gender']}}</td>
                                        <td>{{$value['age']}}</td>
                                        <td>{{$value['internetUsage']}}</td>
                                        <td>{{$value['regesteredOn']}}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Top 10 frequent users </h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content">

                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Phone</th>
                                                        <th>Occupation</th>
                                                        <th>Gender</th>
                                                        <th>Age</th>
                                                        <th>Total sessions</th>
                                                        <th>Most Visted Place</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      @foreach($frequentUser as $value)
                                                    <tr>
                                                        <td>{{$value['name']}}</td>
                                                        <td>{{$value['phone']}}</td>
                                                        <td>{{$value['occupation']}}</td>
                                                        <td>{{$value['gender']}}</td>
                                                        <td>{{$value['age']}}</td>
                                                        <td>{{$value['totalSessionsPeruser']}}</td>
                                                        <td>{{$value['mostVistedPlase']}}</td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
  </div>
        </div>
@endsection

@section('js_dataTable')
<script type="text/javascript" src="{{ URL::asset('dist/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
$(document).ready(function() {
  // if any error occure then
  /*$.fn.dataTable.ext.errMode = 'none';
$('#example1').on('error.dt', function(e, settings, techNote, message) {
 window.location.href="login?sessiontimeout=1";
})*/

   var table = $('#example1').DataTable( {

      "fnDrawCallback": function(settings, json) { // call function when all data is loaded
                var api = this.api();
                api.$('tr').click( function () {
                var formid =  $(this).closest('tr').find('td:first').html();
                $( formid + ":nth-last-child(2)" ).submit();
                //check_session();
                 } );

},
/*"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            if(data == ''){document.getElementById('profil-found').innerHTML=0;}
          },*/
        "processing": true,
        "serverSide": true,
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
        "ajax":{
    url: "serverProcessingController",
    type: 'GET',
  },
  "columns" : [
    {"data":"daterange","dateValue":"daterange","autowidth":true},
      {
           "data": "item",
           "className": "client-avatar"
       }
  ],
        "aoColumns": [
            { "mData": null,
              "mRender": function(edata){

                //document.getElementById('profil-found').innerHTML=edata[8];
                return ' <span>'+edata[0]+'</span>{!! Form::open(array('url'=>'userInfo','method'=>'POST')) !!}<input type="hidden" name="userMac" value="'+edata[7]+'" />{!! Form::close() !!} ';
}
            },
            { "mData": 1},
            { "mData": 2},
            { "mData": 3},
            { "mData": 4},
            { "mData":  5},
            { "mData":  6},
          //  { "mData": 7},
          //  { "mData":  8},
        ]

    } );
    ///////////////////// for datepicker
    $('#reservation').on('change', function(){
      //document.getElementById('profil-found').innerHTML='<i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom" style=" font-size: 14px;"></i>';
      table.columns(0).search( $(this).val() ).draw();
     });
} );
</script>
@endsection
