@extends('dashboard.layouts.dashLayout')
@section('title','Wifigen | Managemen')
@section('ManagementClass','active')
@section('setClass','active')
@section('headear_title','Managemen')
@section('content')
<div class="wrapper wrapper-content">
   <!-- Info boxes -->
   <style>
      .bg{background-color: #f3f3f4;
      border-radius: 14px;
      margin-top: 51px;
      margin-left:20px;
      }
      .alex{
      padding-top:10px}
      .bg {
      background-color: #f3f3f4;
      border-radius: 14px;
      margin-top: 51px;
      margin-left: 20px;
      }
      .profile-image {
      width: 120px;
      float: left;
      font-size: 74px;
      text-align: center;
      margin-top: 10px;
      }
   </style>
   @if(Session::has('success'))
         <div class="alert alert-success alert-dismissable" style="height: 44px;">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
           <h4>	<i class="icon fa fa-check"></i> {!! Session::get('success') !!}</h4>
         </div>
 @endif
 @if(Session::has('danger'))
       <div class="alert alert-danger alert-dismissable" style="height: 44px;">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4>	<i class="icon fa fa-check"></i> {!! Session::get('danger') !!}</h4>
       </div>
@endif
   <div class="row" style="background-color:#FFFFFF;padding-bottom: 50px;">
      <div class="col-md-4 bg" style="border-radius: 3px;">
         <div class="profile-image">
            <img src="dist/uploads/{{Auth::user()->userPic}}" class="img-circle circle-border m-b-md" alt="profile">
         </div>
         <div class="profile-info">
            <div class="alex">
               <div>
                  <h2 class="no-margins">
                     {{Auth::user()->name}}
                  </h2>
                  <p style="text-align:left">@if(Auth::user()->userStaus == 0)Admin @else Limited @endif</p>
                  <p>
                     {{Auth::user()->email}}
                  </p>
               </div>
            </div>
         </div>
         <div class="ibox float-e-margins">
            <div class="ibox-title">
               <h5>Profile Options </h5>
               <div class="ibox-tools">
               </div>
            </div>
            <div class="ibox-content">
              <input type="hidden" name="dlduser" id="confrmBox">
              @if(Session::has('success_pass'))
              <div class="alert alert-success alert-dismissable" style="height: 0px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-top: -7px;margin-right: -9px;">×</button>
                <h4 style="margin-top: -7px;">	<i class="icon fa fa-check"></i> {!! Session::get('success_pass') !!}</h4>
              </div>
      @endif
               <div >
                  <a data-toggle="modal" href="#modal-form" onclick="valuedisable()">Change Password</a>
               </div>
               <div> <a data-toggle="modal" href="#modal-form1">@if(Auth::user()->userPic == 'fakeImg.png')Add your picture @else Change Picture @endif</a> </div>
               <div id="modal-form" class="modal fade" aria-hidden="true">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-body">
                           <div class="row">
                              <div class="col-lg-12">
                                 <h3 class="m-t-none m-b">Change Password</h3>
                                 {!! Form::open(array('url'=>'changePassword','method'=>'POST')) !!}
                                    <div class="form-group"><label>Current Password</label><br />
                                      <span style="color: red; Display:none" id="errorPassword">Your current password is not correct.</span>
                                      <input type="password" id="confrmpassword" autofocus placeholder="Current Password" onblur="confrmPassword()" class="form-control emptyValue"></div>
                                    <div class="form-group">
                                       <label>New Password * </label>
                                       <br>
                                      <span id="password_strength"></span><input id="newPassword" disabled onkeyup="CheckPasswordStrength(this.value)" name="newPassword" placeholder="New Password" type="password" class="form-control enableInput emptyValue required valid" aria-required="true" aria-invalid="false">
                                    </div>
                                    <div class="form-group">
                                       <label>Confirm Password * </label>
                                       <br />
                                       <span style="color: red; Display:none" id="confrmError">Please enter new and confirm password(same) ......</span>
                                       <label id="confirm-error" class="error" for="confirm" style="display: none;"></label><input id="confirm" onkeyup="checkConfrmPassword()" disabled name="confirm" placeholder=" Confirm Password" type="password" class="form-control enableInput emptyValue required valid" aria-required="true" aria-invalid="false">
                                    </div>
                                    <div>
                                       <button id="saveButton" class="btn btn-sm btn-primary enableInput pull-right m-t-n-xs" type="submit" disabled aria-hidden="false" aria-disabled="false"><strong>Save changes</strong></button>
                                    </div>
                                 {!! Form::close() !!}
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!------ image upload ------------------->
               <!------- end image upload ------------->
            </div>
            <div>&nbsp;</div>
            <div class="ibox float-e-margins; col-lg-12">
               <div class="ibox-title">
                  <h5>Account Options </h5>
                  <div class="ibox-tools"></div>
               </div>
               <div class="ibox-content">
                  <div> <a data-toggle="modal" href="#modal-form2">Add Users</a></div>
                  <div id="modal-form2" class="modal fade" aria-hidden="true">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-lg-12">
                                    <h3 class="m-t-none m-b">Add User</h3>
                                    {!! Form::open() !!}
                                    <div class="form-group">
                                       <label>Name</label>
                                       <input type="text" placeholder="Name" name="nameUser" class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label>Email Address </label>
                                       <input id="Email" name="EmailAdress" placeholder="Email Address" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label>Mobile Number</label>
                                       <input id="phone" name="MobileNumber" placeholder="Mobile Number" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label>Password</label>
                                       <input type="password" id="userPassword"  name="userPassword" placeholder=" Password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label>Confirm Password </label>
                                       <input id="ConfirmPassword" placeholder=" ConfirmPassword" type="Password" class="form-control required valid" aria-required="true" aria-invalid="false">
                                    </div>
                                    <div class="form-group">
                                       <div>
                                          <label>Account Type </label>
                                       </div>
                                       <div class="radio radio-info radio-inline">
                                          <input type="radio" id="admin" name="usertype" value="0" name="radioInline">
                                          <label for="inlineRadio2">Admin</label>
                                       </div>
                                       <div class="radio radio-inline">
                                          <input type="radio" id="limited" name="usertype" value="1" name="radioInline" checked="">
                                          <label for="inlineRadio1">Limited</label>
                                       </div>
                                       <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" aria-hidden="false" aria-disabled="false"><strong>Save changes</strong></button>
                                    </div>
                                    {!! Form::close() !!}
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- tab start -->
      <div class="col-lg-7 bg1">
            <div class="clients-list">
               <ul class="nav nav-tabs">
                  <span class="pull-right small text-muted">14 Users</span>
                  <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-group"></i> Manage Users</a></li>
                  <li class=""></li>
               </ul>
               <div class="tab-content">
                  <div id="tab-5" class="tab-pane active">
                     <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                        <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                           <div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                              <div class="table-responsive">
                                 <table class="table table-striped table-hover">
                                    <tbody>
                                       @foreach($totalcustomer as $data)
                                       @if($data->userStaus!=2)
                                       <tr>
                                          <td class="client-avatar"><a href=""><img alt="image" src="dist/uploads/{{ $data->userPic }}"></a> </td>
                                          <td><a data-toggle="tab" href="#contact-2" class="client-link">{{$data->name}}</a></td>
                                          <td class="contact-type"><i class="fa fa-phone"> </i> </td>
                                          <td> {{$data->phone}}</td>
                                          <td> {{$data->email}}</td>
                                          <td class="client-status ">@if($data->userStaus==0)<span class="label label-warning">Admin</span>@else<span class="label label-primary">Limited</span>@endif</td>
                                          <td class="pull-right" style="width: 85px;"><button title="Edit" type="button" value="{{$data->id}}" class=" editButton btn btn-sm btn-white"> <i class="fa fa-pencil"></i> </button><button style="margin-left:5px;" title="Delete" value="{{$data->id}}"  type="button" class="btnDelete btn btn-sm btn-danger" onClick="if(confirm('Are you sure want to delete  {{$data->name}}  user ?'))
document.getElementById('confrmBox').value=1;
else document.getElementById('confrmBox').value=2" > <i class="fa fa-trash-o"></i> </button></td>
                                       </tr>

                                       <tr id="tr{{$data->id}}" style="display:none;">

                                          <td class="client-avatar"><a href=""><img alt="image" src="http://fakeimg.pl/300/"></a> </td>
                                          <td>{!! Form::open(['url' => 'editUser', 'method' => 'post']) !!}<input type="text" style="padding: 6px 7px;font-size: 11px; height: 24px;" value="{{$data->name}}" placeholder="Name" name="editnameUser" class="form-control"></td>
                                          <td class="contact-type"><i class="fa fa-phone"> </i> </td>
                                          <td> <input id="editphone" name="editMobileNumber" value="{{$data->phone}}" style="padding: 6px 7px;font-size: 11px; height: 24px;" placeholder="Mobile Number" type="text" class="form-control"></td>
                                          <td> <input id="editEmail" value="{{$data->email}}" style="padding: 6px 7px;font-size: 11px; height: 24px;" name="editEmail" placeholder="Email" type="text" class="form-control"></td>
                                          <td class="client-status ">
                                             <div class="radio radio-info radio-inline">
                                                <input type="radio" id="admin" @if($data->userStaus==0) checked @endif name="editusertype" value="0" name="radioInline">
                                                <label for="inlineRadio2">Admin</label>
                                             </div>
                                             <div class="radio" style="margin-top: -1px;">
                                                <input type="radio" id="limited" @if($data->userStaus==1) checked @endif  name="editusertype" value="1" name="radioInline" >
                                                <label for="inlineRadio1">Limited</label>
                                             </div>
                                          </td>
                                          <td class="pull-right"><button title="submit" type="submit" value="{{$data->id}}" name="userId" class="btn btn-sm btn-white">Update</button>{!! Form::close() !!}</td>

                                       </tr>
                                       @endif
                                       @endforeach
                                    </tbody>
                                 </table>
                                 <div class="slimScrollBar" style="width: 7px; position: absolute; top: 235px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 365.112px; background: rgb(0, 0, 0);"></div>
                                 <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <!-- tab  end -->
   </div>
   <!-----------------------------------------------------------------------------------hh-------->
   <div id="modal-form1" class="modal fade" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-body">
               <div class="row">



                 <div class="tab-pane rem" id="pro_img">
        {!! Form::open(array('url'=>'image/upload','id'=>'imgForm','method'=>'POST', 'files'=>true)) !!}
                    <div class="box-body">
                        <label for="exampleInputFile">Profile image</label>
  					  <div class="box-body" id="login_img">
              <?php if(Auth::user()->userPic=='fakeImg.png'){?>
  					  <img id="blah" src="dist/uploads/fakeImg.png" alt="your image" style="width: 300px;">
              <?php } else { ?>
              <img id="blah" src="dist/uploads/{{Auth::user()->userPic }}" alt="your image" style="width: 300px;">
              <?php } ?>
            </div>
                        {!! Form::hidden('hs_img',Auth::user()->id) !!}
                        {!! Form::hidden('img_name',Auth::user()->userPic) !!}
                        {!! Form::file('image', array('id'=>'imgvalidation','style'=>'margin-top:10px;'))!!}
  					<div class="box-footer pull-right">
              {!! Form::submit('Update', array('class'=>'btn btn-primary stopForm')) !!}
                    </div>

          </div>
        {!! Form::close() !!}
  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------------------------------------------------------------------------------------------->
</div>
{!! Form::open(['url' => 'dldUser', 'method' => 'post','id'=>'dlduser']) !!}
<input type="hidden" name="dlduser" id="userDldForm">
{!! Form::close() !!}
@endsection
@section('cropImage')
@include('dashboard.charts.userAcountJs')
@endsection
