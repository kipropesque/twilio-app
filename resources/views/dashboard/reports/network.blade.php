@extends('dashboard.layouts.dashLayout')
@section('title','Wifigen | users')
@section('headear_title','User')
@section('networkUser','active')
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
  <div class="row">
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Total network usage</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkData['totalConsumeData']}}</h1>
                          <!--div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                          <small>Total income</small---->
                      </div>
                  </div>
              </div>
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                        <!---span class="label label-info pull-right">Today</span---->
                          <h5>Download Data</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkData['totalDownload']}}</h1>
                          <!--div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                          <small>New orders</small--->
                      </div>
                  </div>
              </div>
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">
                          <h5>Upload data</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkData['totalUpload']}}</h1>
                          <!--div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                          <small>New visits</small--->
                      </div>
                  </div>
              </div>
              <div class="col-lg-3">
                  <div class="ibox float-e-margins">
                      <div class="ibox-title">

                          <h5>Average bandwidth</h5>
                      </div>
                      <div class="ibox-content">
                          <h1 class="no-margins">{{$networkData['averUserData']}}</h1>
                      </div>
                  </div>
      </div>
  </div>
  <div class="row">
          <div class="col-lg-12">
              <div class="ibox float-e-margins">
                  <div class="ibox-title">
                      <h5>Network usage data</h5>
                      <div class="btn-group pull-right">
                     <button data-toggle="dropdown" id="activeData" class="btn btn-default btn-xs dropdown-toggle">Today <span class="caret"></span></button>
                     <ul class="dropdown-menu" style="min-width: 85px;">
                       <li><a  class="consumetData">Today</a></li>
                         <li><a  class="consumetData">Week</a></li>
                         <li><a  class="consumetData">Month</a></li>
                     </ul>
                 </div>
                  </div>
                  <div class="ibox-content">
                     <div id="downUpload" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                  </div>
              </div>
          </div>
</div>
<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Weekly connections overview</h5>
                    <!---div class="btn-group pull-right">
                   <button data-toggle="dropdown" id="activeData" class="btn btn-default btn-xs dropdown-toggle">Today <span class="caret"></span></button>
                   <ul class="dropdown-menu" style="  min-width: 85px;">
                     <li><a  class="getChartdata">Today</a></li>
                       <li><a  class="getChartdata">Week</a></li>
                       <li><a  class="getChartdata">Month</a></li>
                   </ul>
               </div------>
               <span class="pull-right">Total sessions: {{$networkData['totalSessions']}}</span>
                </div>
                <div class="ibox-content">
                   <div id="weeklysessions" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
</div>
        </div>
@endsection
@section('chartJs')
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/c3/c3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/chartJs/Chart.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
@include('dashboard.charts.networkCharts')
@endsection
