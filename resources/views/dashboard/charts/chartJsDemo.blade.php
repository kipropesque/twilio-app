<script>
$(function () {
    var chart;
    chart = new Highcharts.Chart({
      chart: {
        renderTo: "Userchart",
          type: 'areaspline'
      },
      title: {
          text: ''
      },

      //legend: {"reversed":"false","layout":"horizontal","align":"right","verticalAlign":"top","y":-10,"itemStyle":{"fontWeight":"normal"},"borderWidth":0,"symbolRadius":"2px"},
      xAxis: {
          categories: [],

      },

      legend: {
            enabled: false
        },
      yAxis: {
          title: {
              text: ''
          },
          tickPixelInterval: 40,
          allowDecimals: false,
      },
      exporting: {"enabled":false},
      credits: {
          enabled: false
      },
      plotOptions: {
          areaspline: {
              fillOpacity: 0.5
          }
      },
      series:[{
          name: 'users',
          data:[],
          //data:[{y: 10,myData: "10 B"},{y: 40,myData: "40 B"},{y: 8,myData: "8 B"},{y: 7,myData: "0 B"},{y: 5,myData: "5 B"},{y: 21,myData: "20 B"},{y: 12,myData: "34 B"}]
      },
    ]
    });
    $.ajax({
       url: '{{env('BASEURL')}}Userchart/Today',
       type: 'GET',
       async: true,
       dataType: "json",
       success: function (data) {
         var totalUser =data[0]['data'];
         var categoriesArray = data[1]['categories'];
         //console.log(dataArraydownL);
         var arr = categoriesArray,
          twoArray = totalUser;
         chart.series[0].setData(eval(twoArray), false, true);
         chart.xAxis[0].setCategories(arr, true, true);
       }
     });
    $(".getChartdata").click(function () {
      document.getElementById('activeData').innerHTML=$(this).html()+' <span class="caret"></span>';
      var action = $(this).html();
      $.ajax({
         url: '{{env('BASEURL')}}Userchart/' + action,
         type: 'GET',
         async: true,
         dataType: "json",
         success: function (data) {
           var totalUser =data[0]['data'];
           var categoriesArray = data[1]['categories'];
           //console.log(data[2]);
           var arr = categoriesArray,
            twoArray = totalUser;
           chart.series[0].setData(eval(twoArray), false, true);
           chart.xAxis[0].setCategories(arr, true, true);
         }
       });

    });
});
</script>
