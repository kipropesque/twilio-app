<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/cropper/cropper.min.js') }}"></script>
<script>
// image validation
(function($) {
    $.fn.checkFileType = function(options) {
        var defaults = {
            allowedExtensions: [],
            success: function() {},
            error: function() {}
        };
        options = $.extend(defaults, options);

        return this.each(function() {

            $(this).on('change', function() {
                var value = $(this).val(),
                    file = value.toLowerCase(),
                    extension = file.substring(file.lastIndexOf('.') + 1);

                if ($.inArray(extension, options.allowedExtensions) == -1) {
                    options.error();
                    $(this).focus();
                } else {
                    options.success();

                }

            });

        });
    };

})(jQuery);

$(function() {
    $('#inputImage').checkFileType({
        allowedExtensions: ['jpg', 'jpeg','png','gif'],
        success: function() {
          $(".stopForm").removeAttr("disabled");
            //alert('Success');
        },
        error: function() {
          $(".stopForm").attr("disabled", "disabled");
          document.getElementById('displayError').style.display="block";
          //  alert('Error');
        }
    });

});
$('.stopForm').click(function()
{
  var ext = $('#inputImage').val().split('.').pop().toLowerCase();
  if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
  document.getElementById('displayError').style.display="block";
  $("#imgForm").submit(function(e){
    e.preventDefault();
  });
  }
  else{$('#imgForm').unbind('submit').submit();}
})
// end img upload validation
function valuedisable()
{

  $('.emptyValue').val('');
  document.getElementById('password_strength').innerHTML='';
  $('.enableInput').prop('disabled', true);
}
function confrmPassword()
{
  var usersecert= document.getElementById('confrmpassword').value;
  if(usersecert != ''){
  $.ajax({
          url: '{{ url('confrmpassword') }}',
          type: 'post',
          data: {"_token": "{{ csrf_token() }}", usersecert:usersecert},
          success: function(data) {
              if(data != '1true')
              {
                document.getElementById('errorPassword').style.display='block';
                $('.enableInput').prop('disabled', true);
              } else{
                  document.getElementById('errorPassword').style.display='none';
                 $('.enableInput').prop('disabled', false);
                 $('#saveButton').prop('disabled', true);
                $('#newPassword').focus();
               }
          }
      });
    }
}
function CheckPasswordStrength(password) {
            var password_strength = document.getElementById("password_strength");

            //TextBox left blank.
            if (password.length == 0) {
                password_strength.innerHTML = "";
                return;
            }

            //Regular Expressions.
            var regex = new Array();
            regex.push("[A-Z]"); //Uppercase Alphabet.
            regex.push("[a-z]"); //Lowercase Alphabet.
            regex.push("[0-9]"); //Digit.
            regex.push("[$@$!%*#?&]"); //Special Character.

            var passed = 0;

            //Validate for each Regular Expression.
            for (var i = 0; i < regex.length; i++) {
                if (new RegExp(regex[i]).test(password)) {
                    passed++;
                }
            }

            //Validate for length of Password.
            if (passed > 2 && password.length > 8) {
                passed++;
            }

            //Display status.
            var color = "";
            var strength = "";
            var test = 9;
            switch (passed) {
                case 0:
                case 1:
                    strength = "Weak";
                    color = "red";
                    break;
                case 2:
                    strength = "Good";
                    color = "darkorange";
                    break;
                case 3:
                case 4:
                    strength = "Strong";
                    color = "green";
                    break;
                case 5:
                    strength = "Very Strong";
                    color = "darkgreen";
                    break;
            }
            password_strength.innerHTML = strength;
            password_strength.style.color = color;
        }
        function checkConfrmPassword()
        {
          var newPassword = document.getElementById('newPassword').value;
          var confirm = document.getElementById('confirm').value;
          if(newPassword == confirm){ $('#saveButton').prop('disabled', false);document.getElementById('confrmError').style.display="none";}
          else{document.getElementById('confrmError').style.display="block";$('#saveButton').prop('disabled', true);}
        }
$('.btnDelete').on('click', function (e) {
  var userID = $(this).val();
  document.getElementById('userDldForm').value=userID;
  var confrm = document.getElementById('confrmBox').value;
  if(confrm==1){
  $('#dlduser').submit();
}
    });
   $('.editButton').on('click', function (e) {
     e.preventDefault();

          $('#tr'+$(this).val()).toggle('slow');
       });
           $(document).ready(function(){
   var $image = $(".image-crop > img")
               $($image).cropper({
                   aspectRatio: 1.618,
                   preview: ".img-preview",
                   done: function(data) {
                       // Output the result data for cropping image.
                   }
               });

               var $inputImage = $("#inputImage");
               if (window.FileReader) {
                   $inputImage.change(function() {
                       var fileReader = new FileReader(),
                               files = this.files,
                               file;

                       if (!files.length) {
                           return;
                       }

                       file = files[0];

                       if (/^image\/\w+$/.test(file.type)) {
                           fileReader.readAsDataURL(file);
                           fileReader.onload = function () {
                               $inputImage.val("");
                               $image.cropper("reset", true).cropper("replace", this.result);
                           };
                       } else {
                           showMessage("Please choose an image file.");
                       }
                   });
               } else {
                   $inputImage.addClass("hide");
               }

               $("#download").click(function() {
                   window.open($image.cropper("getDataURL"));
               });

               $("#zoomIn").click(function() {
                   $image.cropper("zoom", 0.1);
               });

               $("#zoomOut").click(function() {
                   $image.cropper("zoom", -0.1);
               });

               $("#rotateLeft").click(function() {
                   $image.cropper("rotate", 45);
               });

               $("#rotateRight").click(function() {
                   $image.cropper("rotate", -45);
               });

               $("#setDrag").click(function() {
                   $image.cropper("setDragMode", "crop");
               });


           });


</script>
