<script>

function  downUpload (dataArraydownL,downloadName,dataArrayupL,upLName,categoriesValue) {
$('#downUpload').highcharts({
  chart: {
      type: 'areaspline'
  },
  title: {
      text: ''
  },

  legend: {"reversed":"false","layout":"horizontal","align":"right","verticalAlign":"top","y":-10,"itemStyle":{"fontWeight":"normal"},"borderWidth":0,"symbolRadius":"2px"},
  xAxis: {
      categories: categoriesValue,

  },
  yAxis: {
      title: {
          text: ''
      },
      allowDecimals: false,
  },
  exporting: {"enabled":false},
  tooltip: {
    formatter: function() {
           var s = [];

           $.each(this.points, function(i, point) {
               s.push('<span style="font-weight:bold;">'+ point.series.name +' : '+ this.point.myData +'<span> </ br>');
           });

           return s.join('and ');
       },
      shared: true,
  },
  credits: {
      enabled: false
  },
  plotOptions: {
      areaspline: {
          fillOpacity: 0.5
      }
  },
  series:[{
      name: downloadName,
      data:dataArraydownL,
      //data:[{y: 10,myData: "10 B"},{y: 40,myData: "40 B"},{y: 8,myData: "8 B"},{y: 7,myData: "0 B"},{y: 5,myData: "5 B"},{y: 21,myData: "20 B"},{y: 12,myData: "34 B"}]
  },
  {
    name: upLName,
    data:dataArrayupL,
  }
]
});
};
///// get data from db

$('.consumetData').on('click' , function() {
document.getElementById('activeData').innerHTML=$(this).html()+' <span class="caret"></span>';
var action = $(this).html();
$.ajax({
url: '{{env('BASEURL')}}dataUploadDownload/' + action,
type: 'GET',
async: true,
dataType: "json",
success: function (data) {
  var dataArraydownL = [];
  var dataArrayUpL = [];
  var categoriesArray = data[2]['categories'];
//create chart series format
for(var i=0;i<data[0]['data'].length;i++)
{
  //var valueData =;
var sipraterD =  data[0]['data'][i].split(':');
var sipraterUp =  data[1]['data'][i].split(':');
dataArraydownL.push({y:parseInt(sipraterD[0]),myData:sipraterD[1]});
dataArrayUpL.push({y:parseInt(sipraterUp[0]),myData:sipraterUp[1]});
}
    downUpload(dataArraydownL,data[0]['name'],dataArrayUpL,data[1]['name'],categoriesArray);
}
});
})
$(document).ready(function() {
//document.getElementById('activeData').innerHTML=$(this).html()+' <span class="caret"></span>';
$.ajax({
url: '{{env('BASEURL')}}dataUploadDownload/Today',
type: 'GET',
async: true,
dataType: "json",
success: function (data) {
  var dataArraydownL = [];
  var dataArrayUpL = [];
  var categoriesArray = data[2]['categories'];
//create chart series format
for(var i=0;i<data[0]['data'].length;i++)
{
   //var valueData =;
var sipraterD =  data[0]['data'][i].split(':');
var sipraterUp =  data[1]['data'][i].split(':');
dataArraydownL.push({y:parseInt(sipraterD[0]),myData:sipraterD[1]});
dataArrayUpL.push({y:parseInt(sipraterUp[0]),myData:sipraterUp[1]});
}
    downUpload(dataArraydownL,data[0]['name'],dataArrayUpL,data[1]['name'],categoriesArray);
}
});
  // total connection chart
  $(function () {
      var chart;
      chart = new Highcharts.Chart({
        chart: {
          renderTo: "weeklysessions",
            type: 'areaspline'
        },
        title: {
            text: ''
        },

        //legend: {"reversed":"false","layout":"horizontal","align":"right","verticalAlign":"top","y":-10,"itemStyle":{"fontWeight":"normal"},"borderWidth":0,"symbolRadius":"2px"},
        xAxis: {
            categories: [],

        },

        legend: {
              enabled: false
          },
        yAxis: {
            title: {
                text: ''
            },
            tickPixelInterval: 40,
            allowDecimals: false,
        },
        exporting: {"enabled":false},
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series:[{
            name: 'connections',
            data:[],
            //data:[{y: 10,myData: "10 B"},{y: 40,myData: "40 B"},{y: 8,myData: "8 B"},{y: 7,myData: "0 B"},{y: 5,myData: "5 B"},{y: 21,myData: "20 B"},{y: 12,myData: "34 B"}]
        },
      ]
      });
      $.ajax({
         url: '{{env('BASEURL')}}totalSessions/Week',
         type: 'GET',
         async: true,
         dataType: "json",
         success: function (data) {
           var totalUser =data[0]['data'];
           var categoriesArray = data[1]['categories'];
           var arr = categoriesArray,
            twoArray = totalUser;
           chart.series[0].setData(eval(twoArray), false, true);
           chart.xAxis[0].setCategories(arr, true, true);
         }
       });

    })                                                                                                               })
</script>
