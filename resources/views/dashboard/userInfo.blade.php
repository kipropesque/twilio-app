@extends('dashboard.layouts.dashLayout')
@section('title','Wifigen | user Profilers')
@section('headear_title','User Profile')
@section('user_active','active')
@section('content')
<link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('dist/css/styles.concat.min.css') }}">
<section class="content">
    <!------------------->
    <section class="content client-profile ng-scope">
    <div class="white-stripe">
        <div class="dark-bar">
            <div class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="row">
                        <div class="col-sm-4 col-xs-4">
                            <h6 class="ng-binding" style="font-size:10px;">REGISTERED ON</h6>
                                                        <h1 class="ng-binding">Nov 16 2015</h1>
                        </div>
                        <div class="col-sm-4 col-xs-4">

                            <h6 class="ng-binding" style="font-size:10px;">LAST TIME SEEN</h6>
                                                        <h1 class="ng-binding">Nov 20</h1>
                        </div>

                        <div class="col-sm-4 col-xs-4">
                            <h6 class="ng-binding" style="font-size:10px;">CONNECTIONS</h6>
                            <h1 class="ng-binding">8</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-xs-4">
            <div class="picture">
			<img ng-src="dist/images/no_available_image.gif" src="dist/images/no_available_image.gif">

            </div>

        </div>
        <div class="col-sm-9 col-xs-8">
            <h1 class="mega ng-scope ng-binding" style="margin-left:10px;">  {{$dd(userProfiledata}}             <!--<small ng-if="client.age()" class="ng-scope ng-binding">Age</small>-->

            </h1>
            <dl class="dl-horizontal info hidden-xs">
             <dt class="ng-scope"><i class="fa fa-envelope"></i></dt><dd class="ng-scope ng-binding"><a href="mailto:03222671103">03222671103</a>

            </dd></dl>

        </div>
    </div>
    <div class="row visible-xs">
        <div class="col-xs-12" style="margin-top:20px;">
            <dl class="dl-horizontal info">
                <!--<dt><i class="fa fa-map-marker"></i></dt>-->
                <!--<dd class="ng-binding">Lahore, Pakistan - Pakistan</dd>-->

                <dt><i class="fa fa-envelope"></i></dt>                <dd class="ng-scope ng-binding"><a target="_blank" href="mailto:03222671103">03222671103</a></dd><h2>&nbsp;</h2> <!-- fix for mibile h2 -->
            </dl>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-offset-3 col-lg-9 logins">

            <h2 class="ng-binding">CONNECTIONS</h2>
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">

                        </div>
                        <div class="col-xs-6">

                        </div>
                    </div>
                    <div class="table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th class="ng-binding">When</th>
                                <th class="ng-binding">Through</th>
                                <th class="ng-binding">Mac Address</th>
                                <th class="ng-binding">Ip Address</th>
                                <th class="ng-binding">Access Point</th>

                            </tr>

                            <tr><td>Friday 20th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.18</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Friday 20th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.18</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Thursday 19th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.18</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Thursday 19th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.18</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Thursday 19th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.18</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Wednesday 18th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.11</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Tuesday 17th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.11</td><td>14-CC-20-A6-F5-72</td></tr><tr><td>Monday 16th November 2015</td><td><span class="fa fa-envelope circle"></span></td><td>30-19-66-89-F5-80</td><td>192.168.182.11</td><td>14-CC-20-A6-F5-72</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="dataTables_info ng-scope" id="example2_info" translate="PAGINATION.DESCRIPTION" translate-value-current="1" translate-value-limit="100" translate-value-total="2"></div>
                        </div>
                        <div class="col-xs-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li ng-class="{prev: true, disabled: page == 1}" class="prev disabled"><a ng-click="page = page - 1" class="ng-binding">←
                                        Previous</a></li>
                                    <!-- ngRepeat: i in pageToShow() --><li ng-class="{active: i == page}" ng-repeat="i in pageToShow()" class="ng-scope active">
                                        <a ng-click="page = i" class="ng-binding">1</a>
                                    </li><!-- end ngRepeat: i in pageToShow() -->
                                    <li ng-class="{next: true, disabled: page == page_count}" class="next disabled"><a ng-click="page = page + 1" class="ng-binding">Next → </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>

</section>
    <!---------------------------->
  </section>
@endsection
@section('js_dataTable')
<script type="text/javascript" src="{{ URL::asset('dist/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $(function () {
    $("#example1").dataTable();
    $('#example2').dataTable({
      "bPaginate": true,
      "bLengthChange": false,
      "bFilter": false,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false
    });
  });
</script>
@endsection
