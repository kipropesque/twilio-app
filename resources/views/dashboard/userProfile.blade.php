@extends('dashboard.layouts.dashLayout')
@section('title','Wifigen | user Profilers')
@section('headear_title','User Profile')
@section('user_active','active')
@section('content')
<link media="all" type="text/css" rel="stylesheet" href="{{ URL::asset('dist/css/styles.concat.min.css') }}">
<section class="content">
    <!------------------->
    <section class="content client-profile ng-scope">
    <div class="white-stripe">
        <div class="dark-bar">
            <div class="row">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="row">
                        <div class="col-sm-4 col-xs-4">
                            <h6 class="ng-binding" style="font-size:10px;">REGISTERED ON</h6>
                                                        <h1 class="ng-binding">{{$userInfoBasic[0]['registerOn']}}</h1>
                        </div>
                        <div class="col-sm-4 col-xs-4">

                            <h6 class="ng-binding" style="font-size:10px;">LAST TIME SEEN</h6>
                                                        <h1 class="ng-binding">{{$userInfoBasic[0]['lastTimeSession']}}</h1>
                        </div>

                        <div class="col-sm-4 col-xs-4">
                            <h6 class="ng-binding" style="font-size:10px;">CONNECTIONS</h6>
                            <h1 class="ng-binding">{{$userInfoBasic['totalSessions']}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-xs-4">
            <div class="picture">
              @if($userInfoBasic[0]['profilePict'] != null)<img  src="{{$userInfoBasic[0]['profilePict']}}">
              @else
			           <img ng-src="dist/images/no_available_image.gif" src="dist/images/no_available_image.gif">
                 @endif

            </div>

        </div>
        <div class="col-sm-9 col-xs-8">
            <h1 class="mega ng-scope ng-binding" style="margin-left:10px;">  {{$userInfoBasic[0]['Name']}}            <!--<small ng-if="client.age()" class="ng-scope ng-binding">Age</small>-->

            </h1>
            <dl class="dl-horizontal info hidden-xs">
             <dt class="ng-scope"><i class="fa fa-envelope"></i></dt><dd class="ng-scope ng-binding"><a href="mailto:{{$userInfoBasic[0]['Name']}}">{{$userInfoBasic[0]['email']}}</a>

            </dd></dl>

        </div>
    </div>
    <div class="row visible-xs">
        <div class="col-xs-12" style="margin-top:20px;">
            <dl class="dl-horizontal info">
                <!--<dt><i class="fa fa-map-marker"></i></dt>-->
                <!--<dd class="ng-binding">Lahore, Pakistan - Pakistan</dd>-->

                <dt><i class="fa fa-envelope"></i></dt>                <dd class="ng-scope ng-binding"><a target="_blank" href="mailto:03222671103">03222671103</a></dd><h2>&nbsp;</h2> <!-- fix for mibile h2 -->
            </dl>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-offset-3 col-lg-9 logins">

            <h2 class="ng-binding">CONNECTIONS</h2>
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">

                        </div>
                        <div class="col-xs-6">

                        </div>
                    </div>
                    <div class="table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th class="ng-binding">When</th>
                                <th class="ng-binding">Through</th>
                                <th class="ng-binding">Device Mac</th>
                                <th class="ng-binding">session Time</th>
                                <th class="ng-binding">SSID</th>

                            </tr>
                            @foreach($userInfoFull as $value)
                            <tr><td>{{$value['when']}}</td><td><span class="fa fa-{{$value['authLogin']}} circle"></span></td><td>{{$value['macAdress']}}</td><td>{{$value['sessionTime']}}</td><td>{{$value['ssid']}}</td></tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="dataTables_info ng-scope" id="example2_info" translate="PAGINATION.DESCRIPTION" translate-value-current="1" translate-value-limit="100" translate-value-total="2"></div>
                        </div>
                        <div class="col-xs-6">
                            <div class="dataTables_paginate paging_bootstrap">
                                <ul class="pagination">
                                    <li ng-class="{prev: true, disabled: page == 1}" class="prev disabled"><a ng-click="page = page - 1" class="ng-binding">←
                                        Previous</a></li>
                                    <!-- ngRepeat: i in pageToShow() --><li ng-class="{active: i == page}" ng-repeat="i in pageToShow()" class="ng-scope active">
                                        <a ng-click="page = i" class="ng-binding">1</a>
                                    </li><!-- end ngRepeat: i in pageToShow() -->
                                    <li ng-class="{next: true, disabled: page == page_count}" class="next disabled"><a ng-click="page = page + 1" class="ng-binding">Next → </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>

</section>
    <!---------------------------->
  </section>
@endsection
@section('js_dataTable')
<script type="text/javascript" src="{{ URL::asset('dist/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $(function () {
    $("#example1").dataTable();
    $('#example2').dataTable({
      "bPaginate": true,
      "bLengthChange": false,
      "bFilter": false,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false
    });
  });
</script>
@endsection
