@extends('dashboard.layouts.dashLayout')
@section('title','PhoneApp | dashboard')
@section('PhoneApp','active')
@section('headear_title','PhoneApp | Dashboard')
@section('content')
<script type="text/javascript"
  src="//media.twiliocdn.com/sdk/js/client/v1.3/twilio.min.js"></script>
<script type="text/javascript"
  src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">
</script>
<link href="{{ URL::asset('dist/css/client.css',true) }}"
  type="text/css" rel="stylesheet" />
<script type="text/javascript">
var CallAccept=function(){};
  Twilio.Device.setup("<?php echo $token; ?>");
  Twilio.Device.ready(function (device) {
    $("#log").text("Ready");
  });
  Twilio.Device.error(function (error) {
    $("#log").text("Error: " + error.message);
$(".table-responsive").html("");
  });
  Twilio.Device.connect(function (conn) {
    $("#log").text("Successfully established call");
$("#call").hide();
  });
  Twilio.Device.disconnect(function (conn) {
    $("#log").text("Call ended");
$(".table-responsive").html("");
$("#call").attr("onclick","call()");
$("#call").show();
document.getElementById("call").innerHTML = "Call";
document.getElementById("call").style.background="#ececec";
  });
Twilio.Device.cancel(function(conn) {
$("#call").attr("onclick","call()");
$("#call").show();
document.getElementById("call").innerHTML = "Call";
document.getElementById("call").style.background="#ececec";
$("#log").text("Call ended By " +conn.parameters.From);
$(".table-responsive").html("");
setTimeout(function() { $("#log").text("Ready"); }, 5000);       
 console.log("Call Ended by "+conn.parameters.From); // who canceled the call
        conn.status // => "closed"
    });
  Twilio.Device.incoming(function (conn) {
CallAccept=conn;
$("#call").attr("onclick","CallAccept.accept()");
document.getElementById("call").innerHTML = "Accept";
document.getElementById("call").style.background="#31e61b";
$.ajax({    //create an ajax request to load_page.php
        type: "GET",
        url: "business/" + conn.parameters.From,             
        dataType: "html",   //expect html to be returned                
        success: function(response){                    
            $(".table-responsive").html(response); 
            //alert(response);
        }

    });

    $("#log").text("Incoming connection from " + conn.parameters.From);
    // accept the incoming connection and start two-way audio


//conn.accept();

  });
  function call() {
    // get the phone number to connect the call to
    params = {"PhoneNumber": $("#number").val()};
    Twilio.Device.connect(params);
  }
  function hangup() {
    Twilio.Device.disconnectAll();
  }
</script>

<div class="wrapper wrapper-content">



       <div class="row phonebox">
         <body>
           <button class="call" id="call" onclick="call();">
             Call
           </button>

           <button class="hangup" onclick="hangup();">
             Hangup
           </button>

           <input type="text" id="number" name="number"
             placeholder="Enter a phone number to call"/>

           <div id="log">Loading pigeons...</div>
<div class="table-responsive"></div>

         </body>
               </div>


 </div>

               </div>
@endsection
