@extends('dashboard.layouts.dashLayout')
@section('title','PhoneApp | dashboard')
@section('dash_active','active')
@section('headear_title','PhoneApp | Dashboard')
@section('content')
@include('dashboard.common.phone')
<div class="wrapper wrapper-content">
       <div class="row">
                   <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                           <div class="ibox-title">
                               <h5>Missed Calls</h5>
                           </div>
                           <div class="ibox-content">
                               <h1 class="no-margins"></h1>
                               <!--div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div--->
                               <small>Missed Calls</small>
                           </div>
                       </div>
                   </div>
                   <style>
                   .dropdown-menu > li > a{ line-height: 12px;}
                   </style>
                   <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                           <div class="ibox-title">
                             <div class="btn-group pull-right">
                            <!--button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">All <span class="caret"></span></button>
                            <ul class="dropdown-menu" style="  min-width: 85px;">
                                <li><a href="#">All</a></li>
                                <li><a href="#">Today</a></li>
                                <li><a href="#">Week</a></li>
                                <li><a href="#">Month</a></li>
                            </ul--->
                        </div>
                               <h5>Total Calls</h5>
                           </div>
                           <div class="ibox-content">
                               <h1 class="no-margins"></h1>
                               <!--div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div---->
                               <small>Total Calls</small>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                           <div class="ibox-title">
                               <h5>Inbound Calls</h5>
                           </div>
                           <div class="ibox-content">
                               <h1 class="no-margins"></h1>
                               <!---div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div----->
                               <small>Inbound Calls</small>
                           </div>
                       </div>
                   </div>
                   <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                           <div class="ibox-title">
                               <h5>Outbound Cals</h5>
                           </div>
                           <div class="ibox-content">
                               <h1 class="no-margins"></h1>
                               <!---div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div---->
                               <small>Outbound calls</small>
                           </div>
                       </div>
           </div>
       </div>
       <div class="row" id="testId">
       </div>
       <!---div class="row">
               <div class="col-lg-12">
                 <div id="container" style="width: 800px; height: 400px; margin: 0 auto"></div>
               </div>
             </div------>
       <div class="row">
               <div class="col-lg-12">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <h5>overview</h5>
                           <div class="btn-group pull-right">
                          <button data-toggle="dropdown" id="activeData" class="btn btn-default btn-xs dropdown-toggle">Today <span class="caret"></span></button>
                          <ul class="dropdown-menu" style="  min-width: 85px;">
                            <li><a  class="getChartdata">Today</a></li>
                              <li><a  class="getChartdata">Week</a></li>
                              <li><a  class="getChartdata">Month</a></li>
                          </ul>
                      </div>
                       </div>
                       <div class="ibox-content">
                          <div id="Userchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                       </div>
                   </div>
               </div>


 </div>
 <!---div class="row">
 <div class="col-lg-4">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <h5>Pie </h5>

                       </div>
                       <div class="ibox-content">
                           <div>
                               <canvas width="427" style="width: 427px; height: 199px;" id="doughnutChart" height="250"></canvas>
                           </div>
                       </div>
                   </div>
               </div>
       <div class="col-lg-4">
                   <div class="ibox float-e-margins">
                       <div class="ibox-title">
                           <h5>Radar Chart Example</h5>
                       </div>
                       <div class="ibox-content">
                           <div >
                               <div id="pie"  style="height: 180px;"></div>
                           </div>
                       </div>
                   </div>
               </div>
       <div class="col-lg-4">
                       <div class="ibox">
                           <div class="ibox-content" style=" border-width: 3px 0;">
                               <h5>Percentage division</h5>
                               <h2>100/54</h2>
                               <div class="text-center">
                                   <div id="sparkline6" style="height: 165px;"></div>
                               </div>
                           </div>
                       </div>
                   </div>
 </div-------->
               </div>
@endsection
@section('chartJs')
<!-- d3 and c3 charts -->
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/d3/d3.min.js',true) }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/c3/c3.min.js',true) }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/chartJs/Chart.min.js',true) }}"></script>
@include('dashboard.charts.chartJsDemo')
@endsection
