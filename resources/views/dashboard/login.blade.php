
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PhoneApp | Login</title>
    <!-- <link rel="shortcut icon" href="{{ asset('favicon.png') }}"> -->
    <link href="{{ asset('dist/css/bootstrap.min.css',true) }}" rel="stylesheet">
    <link href="{{ asset('dist/font-awesome/css/font-awesome.css',true) }}" rel="stylesheet">
    <link href="{{ asset('dist/css/animate.css',true) }}" rel="stylesheet">
    <link href="{{ asset('dist/css/style.css',true) }}" rel="stylesheet">
  </head>
  <body class="gray-bg">

      <div class="middle-box text-center loginscreen animated fadeInDown">
          <div>
              <div>

                <!-- {!!Html::image('dist/images/dash_logo.png','Your logo',array('style'=>'width: 182px;'))!!} -->

              </div>
              <?php
          if (isset($_REQUEST['sessiontimeout'])){
            echo '
            <div class="alert alert-danger" style="margin-top: 26px;">
                    <h5>Session timeout (Please login again)</h5>
                  </div>';
          }
        ?>
        @if (session('status'))
    <div class="alert alert-danger" style="margin-top: 26px;">
        {{ session('status') }}
    </div>
@endif
                {!! Form::open(array('url' =>  URL::to('checkUser',array(),true),'class' => 'm-t')) !!}
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <div class="form-group">
                        {!! Form::text('user_mail','',array('class'=>'form-control','placeholder'=>'write your user name'))!!}
                  </div>
                  <div class="form-group">
                    <?php
              echo Form::password('user_pass', array('class' => 'form-control','placeholder'=>'password'));
               ?>
                  </div>
                  <button type="submit" class="btn btn-primary block full-width m-b">Sign In</button>

                  <!---a href="#"><small>Forgot password?</small></a>
                  <p class="text-muted text-center"><small>Do not have an account?</small></p>
                  <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a---->
              </form>
              <p class="m-t"> <small>Paksoft18 &copy; 2017</small> </p>
          </div>
      </div>

      <!-- Mainly scripts -->

<!---------------------------------------------------->
    <!-- jQuery 2.1.4 -->
      </body>
</html>
