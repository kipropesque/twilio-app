<nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
<!-- {!!Html::image('dist/images/logo.png', 'a logo', array('style' => 'height:30px;margin:5px'))!!} -->
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> <!---span class="text-muted text-xs block"><b class="caret"></b></span> </span----> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <!--li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li----->
                            <li><a href="auth/logout">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        <!-- {!!Html::image('dist/images/logo.png', 'a logo', array('style' => 'height:30px;margin:5px'))!!} -->
                    </div>
                    <div class="logo-element">

                    </div>
                </li>
                <li class=" @yield('dash_active')">
                    <a href="{{url('dash')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Overview</span> </a>

                </li>
                <li class="@yield('PhoneApp')">
                    <a href="{{url('PhoneApp')}}"><i class="fa fa-users"></i> <span class="nav-label">Phone App</span></a>
                </li>
                <li class="@yield('Incoming')">
                    <a href="{{url('Incoming')}}"><i class="fa fa-server"></i> <span class="nav-label">Incoming Calls</span></a>
                </li>

                <li class="@yield('Outgoing')">
                    <a href="{{url('Outgoing')}}"><i class="fa fa-server"></i> <span class="nav-label">Outgoing Calls</span></a>
                </li>
                <li class="@yield('voicemail')">
                    <a href="{{url('voicemail')}}"><i class="fa fa-server"></i> <span class="nav-label">Voicemail</span></a>
                </li>
 
                <!--li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i>     <span class="nav-label">Reports</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="graph_flot.html">Flot Charts</a></li>
                        <li><a href="graph_morris.html">Morris.js Charts</a></li>

                    </ul>
                </li---->
                <!--li class=" @yield('ManagementClass')">
                   <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Management</span><span class="fa arrow"></span></a>
                   <ul class="nav nav-second-level collapse">
                       <li class=" @yield('setClass')"><a href="{{env('BASEURL')}}setting">Users acount</a></li>
                       <li class="@yield('guClass')"><a href="{{env('BASEURL')}}guestUser">Guest Users</a></li>
                   </ul>
               </li---->


        </div>
    </nav>
