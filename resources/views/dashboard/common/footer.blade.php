<div class="footer" style="position:fixed;">

            <div>
                <strong>Copyright</strong> Paksoft18 &copy; 2017
            </div>
        </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    {!!HTML::script('dist/js/jquery-2.1.1.js',array(),true)!!}
    {!!HTML::script('dist/js/bootstrap.min.js',array(),true)!!}
    {!!HTML::script('dist/js/plugins/metisMenu/jquery.metisMenu.js',array(),true)!!}
    @yield('cropImage')
    {!!HTML::script('dist/js/plugins/slimscroll/jquery.slimscroll.min.js',array(),true)!!}

   
    <!-- Custom and plugin javascript -->
    {!!HTML::script('dist/js/inspinia.js',array(),true)!!}
    {!!HTML::script('dist/js/plugins/pace/pace.min.js',array(),true)!!}
    {!!HTML::script('dist/js/jquery.timepicker.min.js',array(),true)!!}
    {!!HTML::script('dist/js/custom.js',array(),true)!!}
    <!-- jQuery UI -->
    {!!HTML::script('dist/js/plugins/jquery-ui/jquery-ui.min.js',array(),true)!!}


@yield('js_dataTable')
@yield('guestUserJs')
