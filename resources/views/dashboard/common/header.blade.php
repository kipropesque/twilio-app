<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title')</title>
    @yield('datePickerCss')
    {!!Html::style('dist/css/bootstrap.min.css',array(),true)!!}
    {!!Html::style('dist/font-awesome/css/font-awesome.css',array(),true)!!}
    {!!Html::style('dist/css/animate.css',array(),true)!!}
    {!!Html::style('dist/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',array(),true)!!}
    {!!Html::style('dist/css/style.css',array(),true)!!}
    {!!Html::style('dist/css/plugins/cropper/cropper.min.css',array(),true)!!}
    {!!Html::style('dist/css/jquery.timepicker.css',array(),true)!!}
    @yield('cssuserFordatepicker')

</head>
