@extends('dashboard.layouts.dashLayout')
@section('title','PhoneApp | Voicemail')
@section('headear_title','Voicemail')
@section('content')

<div class="wrapper wrapper-content  animated fadeInRight">
  <div class="row">
  <div class="about-section">
   <div class="text-content">
      <div class="span7 offset1">
        @if(Session::has('success'))
          <div class="alert-box success">
          <h2>{!! Session::get('success') !!}</h2>
          </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div>
        @endif
          {!! Form::open(array('class'=>'form-horizontal', 'url'=>'editsetting/'.$setting->id,'method'=>'POST', 'files'=>true)) !!}
          <fieldset>
          <!-- Form Name -->
          <legend>Voicemail configration</legend>

          <!-- File Button --> 
          <div class="form-group">
            <label class="col-md-4 control-label" for="filebutton">Voice file</label>
            <div class="col-md-4">
            {{ $setting->voicemailpath }}
              <input id="filebutton" name="voicefile" class="input-file" type="file" >
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="seconds">Time before voicemail</label>
            <div class="col-md-2">
            <input id="seconds" name="seconds" type="text" class="form-control input-md" value="{{ $setting->delay }}">
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="sttime">Start time</label>
            <div class="col-md-2">
            <input id="datetime1" name="sttime" type="text" class="form-control input-md time ui-timepicker-input" value="{{ $setting->start_time }}">
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="endtime">End time</label>
            <div class="col-md-2">
            <input id="datetime2" name="endtime" type="text"  class="form-control input-md time ui-timepicker-input" value="{{ $setting->end_time }}">
            </div>
          </div>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email to send recording</label>
            <div class="col-md-4">
            <input id="email" name="email" type="text" class="form-control input-md" value="{{ $setting->email }}">
            </div>
          </div>

          <!-- Button -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-2">
              <button id="submit" name="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
            <div class="col-md-2">
              <a href="{{url('voicemail')}}" id="cancel" name="cancel" class="btn btn-default">Cancel</a>
            </div>
          </div>
          </fieldset>
          </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
