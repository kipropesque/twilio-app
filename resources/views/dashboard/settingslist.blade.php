@extends('dashboard.layouts.dashLayout')
@section('title','PhoneApp | Voicemail')
@section('headear_title','Voicemail')
@section('content')

<div class="wrapper wrapper-content  animated fadeInRight">
  <div class="row">
  <div class="about-section">
   <div class="text-content">
      <div class="span7 offset1">
        @if(Session::has('success'))
          <div class="alert-box success">
          <h2>{!! Session::get('success') !!}</h2>
          </div>
        @endif
        <fieldset>
          <!-- Form Name -->
          <legend>Voicemail settings list</legend>
          <a class="btn btn-primary" href="{{url('newsetting')}}" role="button">New setting</a>
          <br/>
        <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>voicemail</th>
            <th>Start time</th>
            <th>End time</th>
            <th colspan="2">Delay</th>
          </tr>
        </thead>
        <tbody>
          @forelse($settings as $key => $setting)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{ $setting->voicemailpath }}</td>
            <td>{{ $setting->start_time }}</td>
            <td>{{ $setting->end_time }}</td>
            <td>{{ $setting->delay }}</td>
            <td><a href="{{url('editsetting/'.$setting->id )}}"> Edit</a></td> 
          </tr>
          @empty
            <tr>
              <td colspan="6">No settings set up</td>
            </tr>
          @endforelse
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
