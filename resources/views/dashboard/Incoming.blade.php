@extends('dashboard.layouts.dashLayout')
@section('title','PhoneApp | Incoming')
@section('headear_title','Incoming')
@section('Incoming','active')
@section('content')
@include('dashboard.common.phone')
<div class="wrapper wrapper-content  animated fadeInRight">
  <div class="row">

 <h3>Incoming Calls log</h3>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>DATE</th>
                    <th>DIRECTION</th>
                    <th>FROM</th>
                    <th>TO</th>
                    <th>STATUS</th>
                    <th>DURATION</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($calls as $call)
                @if ($call->direction == 'inbound')
                <tr>
                    <td>{{ $call->startTime->format("Y-m-d H:i:s") }}</td>
                    <td>Incoming</td>
                    <td>{{ $call->from }}</td>
                    <td>{{ $call->to }}</td>
                    <td>{{ $call->status }}</td>
                    <td>{{ $call->duration}}</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>

</div>
</div>
@endsection
@section('chartJs')
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/c3/c3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/plugins/chartJs/Chart.min.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
@include('dashboard.charts.networkCharts')
@endsection
